#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct buf_elem {
   long mtype;
   int mvalue;
};

int main(){
    int msgid;
    struct buf_elem elem;

    msgid = msgget(45281, IPC_CREAT|IPC_EXCL|0600);
    if (msgid == -1){
        msgid = msgget(45281, IPC_CREAT|0600);
        if (msgid == -1){
            perror("Utworzenie kolejki komunikatow");
            exit(1);
        }
    }
    for (int i=0; i<100; i++){
        if (msgrcv(msgid, &elem, sizeof(elem.mvalue),2, 0) == -1){
            perror("Wyslanie pustego komunikatu");
            exit(1);
        }
        printf("%d \n", elem.mvalue);
    }
}