#include <signal.h>
void f(int sig_num){
	printf("Przechwycenie sygnalu nr %d\n", sig_num);
}

main(){
	printf("Domyslna obsluga sygnalu\n");
	signal(SIGINT, SIG_DFL);
	sleep(5);
	printf("Ignorowanie sygnalu\n");
	signal(SIGINT, SIG_IGN);
	sleep(5);
	printf("Przechwytywanie sygnalu\n");
	signal(SIGINT, f);
	sleep(5);
}
