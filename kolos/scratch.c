#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>

struct BUF {
    long mtype;
    int mvalue;
};
#define msize sizeof(struct BUF) - sizeof(long)
int main(){
    int semid, x = 2;
    struct sembuf operacja = {0, 0, 0};
    semid = semget(IPC_PRIVATE, 1, 0600);
    semctl(semid, 0, SETVAL, (int)1);
    if (fork() > 0){
        x++;
        printf("x1=%d\n", x);
        operacja.sem_op = +4;
        semop(semid, &operacja, 1);
        sleep(1);
        wait(NULL);
        printf("x2=%d\n", x);
    }
    else{
        operacja.sem_op = -1;
        semop(semid, &operacja, 1);
        x+=2;
        printf("x3=%d\n", x);
        semop(semid, &operacja, 1);
        x+=3;
        printf("x4=%d\n", x);
    }
}

//
//int main() {
//    int id;
//    struct BUF mb;
//    id = msgget(IPC_PRIVATE, 0600|IPC_CREAT);
//    if ( fork() == 0 ){
//        printf("A");
//        mb.mtype = 1; mb.mvalue = 10;
//        msgsnd(id, &mb, msize, 0);
//        printf("B");
//        mb.mtype = 2; mb.mvalue = 20;
//        msgsnd(id, &mb, msize, 0);
//        printf("C");
//        msgrcv(id, &mb, msize, 4, 0);
//        printf("D");
//        msgrcv(id, &mb, msize, -3, 0);
//        printf("E");
//    }
//    else{
//        printf("F");
//        msgrcv(id, &mb, msize, -2, 0);
//        printf("G");
//        mb.mtype = 3; mb.mvalue = 30;
//        msgsnd(id, &mb, msize, 0);
//        printf("H");
//        mb.mtype = 4; mb.mvalue = 40;
//        msgsnd(id, &mb, msize, 0);
//        printf("I");
//    }
//}