import glob
import subprocess

responses = {}
responses_list = []

def count_res(key):
    if key in responses:
        responses[key] += 1
    else:
        responses[key] = 1


try:
    for _ in range(100000):
        response = subprocess.getoutput('./scratch.o')

        count_res(response)
        if response not in responses_list:
            responses_list.append(response)

except KeyboardInterrupt:
    pass

print(responses_list)
print("Status: " + str(responses))


