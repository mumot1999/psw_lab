#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#define MAX 512

int main(int argc, char* argv[]){
   char buf[MAX];
   int desc_zrod, desc_cel;
   int lbajt;
   int cnt = 0;
 
   if (argc<3){
      fprintf(stderr, "Za malo argumentow. Uzyj:\n");
      fprintf(stderr, "%s <plik zrodlowy> <plik_docelowy>\n", argv[0]);
      exit(1);
   }

   desc_zrod = open(argv[1], O_RDONLY);
   if (desc_zrod == -1){
      perror("Blad otwarcia pliku zrodlowego");
      exit(1);
   }

   desc_cel = creat(argv[2], 0640);
   if (desc_cel == -1){
      perror("Blad utworzenia pliku docelowego");
      exit(1);
   }

   fseek(desc_zrod,0,SEEK_END);
   cnt = ftell(desc_zrod);

	int i = 0;
    while( i < cnt )
    {
        i++;
        fseek(desc_zrod,-i,SEEK_END);
        printf("%c",fgetc(desc_zrod));
    }

   if (lbajt == -1){
      perror("Blad odczytu pliku zrodlowego");
      exit(1);
   }

   if (close(desc_zrod) == -1 || close(desc_cel) == -1){
      perror("Blad zamkniecia pliku");
      exit(1);
   }

   exit(0);
}
