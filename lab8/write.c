#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define MAX 2

main(){
   int shmid, i;
   volatile int *buf;
   
   shmid = shmget(45289, MAX*sizeof(int), IPC_CREAT|0600);
   if (shmid == -1){
      perror("Utworzenie segmentu pamieci wspoldzielonej");
      exit(1);
   }
   
   buf = (int*)shmat(shmid, NULL, 0);
   if (buf == NULL){
      perror("Przylaczenie segmentu pamieci wspoldzielonej");
      exit(1);
   }
   buf[1] = 0;

   if(fork() == 0)
   {
      for (i=0; i<100; i++){
         while(buf[1] != 1){
         }
         printf("%d\n", buf[0]);
         buf[1] = 0;
      }
   }else{
      for (i=0; i<100; i++){
         while(buf[1] != 0){
         }
         buf[0] = i;
         buf[1] = 1;
      }
   }

}
