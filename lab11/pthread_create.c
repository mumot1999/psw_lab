#include <stdio.h>
#include <pthread.h>


void* f(void* p){
    int i = 1;
    static int j = 0;
    printf("%d, %d \n", ++i, ++j);
    return NULL;
}

int main(){

    int thread_count = 400;
    pthread_t th_ids[thread_count];
    
    for(int i = 0; i < thread_count; i++)
        pthread_create(&th_ids[i], NULL, f, NULL);
    
    for(int i = 0; i < thread_count; i++)
        pthread_join(th_ids[i], NULL);
}

