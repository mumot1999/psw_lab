#include <stdio.h>
#include <pthread.h>
unsigned int seed;

void* f(void* p){
    int i, r;
    pthread_t my_id;
    my_id = pthread_self();
    for(i = 0; i < 10; i++){
        r = rand_r(&seed);
        printf("id: %d, %d \n", my_id, r);
    }
}

int main(){
    pthread_t thread1, thread2;
    pthread_create(&thread1, NULL, f, NULL);
    pthread_create(&thread2, NULL, f, NULL);
    sleep(1);
}
