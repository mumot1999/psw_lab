 #include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

struct Consumer_s
{
    int id;
    int weight;
    int last_weight;
    int is_eating;
    int want_to_eat;

} Consumer_default = {0, 0, 0, 0, 1};

typedef struct Consumer_s Consumer;


int can_eat(int id);
