 #include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include "consumer.h"

#define COUNT 5

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int consumers_count = COUNT;
Consumer* consumers;

Consumer get_consumer(int id){
    if (id < 0){
        id += consumers_count;
    }

    return consumers[id % consumers_count];
}

int can_eat(int id){

    Consumer consumer = get_consumer(id);
    Consumer left = get_consumer(id-1);
    Consumer right = get_consumer(id+1);

    int priority_left = 0;
    int priority_right = 0;

    if(left.is_eating || right.is_eating)
        return 0;

    if(left.want_to_eat){
        if(left.last_weight == consumer.last_weight){
            if(left.id < consumer.id)
                priority_left = 1;
            else
                priority_left = 0;
        } else if (left.last_weight < consumer.last_weight){
            priority_left = 1;
        }
    }

    if(right.want_to_eat){
        if(right.last_weight == consumer.last_weight){
            if(right.id < consumer.id)
                priority_right = 1;
            else
                priority_right = 0;
        } else if (right.last_weight < consumer.last_weight){
            priority_right = 1;
        }
    }

    if(priority_left)
        priority_left = can_eat(left.id);
    if(priority_right)
        priority_right = can_eat(right.id);

//    printf("priority %d, %d-%d \n", id, priority_left, priority_right);
    return !(priority_left || priority_right);
}

void run_consumer(int id){
    int food = (id % 5) + 1;

    if(can_eat(id)){
        consumers[id].is_eating = 1;
        sleep(3);
        consumers[id].weight += food;
        consumers[id].is_eating = 0;
        consumers[id].last_weight = consumers[id].weight;

        consumers[id].want_to_eat = 0;
        sleep(2);
        consumers[id].want_to_eat = 1;

    }

}

 void print_consumers(){
//     for(int i = 0; i < consumers_count ; i++)
//         printf("%d: %d \t\t- %s\n", i, consumers[i].weight, consumers[i].is_eating ? "eating" : "waiting");
//     printf("-------------------\n");

     for(int i = 0; i < consumers_count ; i++)
        printf("%4d %s \t", consumers[i].weight, consumers[i].is_eating ? ANSI_COLOR_GREEN"E"ANSI_COLOR_RESET : !consumers[i].want_to_eat ? ANSI_COLOR_RED"W"ANSI_COLOR_RESET : "");
     printf("\n");
     usleep(500000);
 }

 int main(){

     int shmid = shmget(45289, (consumers_count)*sizeof(Consumer), IPC_CREAT|0600);
     if (shmid == -1){
         perror("Utworzenie segmentu pamieci wspoldzielonej");
         exit(1);
     }

     consumers = (Consumer*)shmat(shmid, NULL, 0);
     if (consumers == NULL){
         perror("Przylaczenie segmentu pamieci wspoldzielonej");
         exit(1);
     }

     for(int i = 0; i < consumers_count ; i++) {
         consumers[i] = Consumer_default;
         consumers[i].id = i;
         consumers[i].want_to_eat = 1;
     }

     for(int i = 0; i < consumers_count ; i++){

         if(fork() == 0){
             consumers = (Consumer*)shmat(shmid, NULL, 0);
             if (consumers == NULL){
                 perror("Przylaczenie segmentu pamieci wspoldzielonej");
                 exit(1);
             }
             while (1)
                 run_consumer(i);
         }
     }

     while(1) {
         print_consumers();
     }
 }