Do portu zawijają statki, cumują w nim przez jakiś czas i go opuszczają. Każdy statek zarówno w celu wejścia do portu, jak i wyjścia z niego musi dostać pewną liczbę holowników, zależną od jego masy. Musi być też dla niego miejsce w doku. Zarówno liczba miejsc w doku, jak i liczba holowników są ustalone i ograniczone.

Cel zadania: synchronizacja statków (doki i holowniki to zasoby)

