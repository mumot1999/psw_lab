#include <stdio.h>
#include <pthread.h>

#define COUNT 5

#define FOREACH_STATUS(STATUS) \
        STATUS(dock)   \
        STATUS(swimToDock)  \
        STATUS(waitingInDock)  \
        STATUS(waitingForDock)   \
        STATUS(out)\
        STATUS(swimmingOut)\

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum status {
    FOREACH_STATUS(GENERATE_ENUM)
};

static const char *STATUS_STRING[] = {
        FOREACH_STATUS(GENERATE_STRING)
};

struct Ship_s {
    int id;
    int tugs_count;
    enum status status;
} default_Ship = {0, 0, out};

typedef struct Ship_s Ship;

int dock_size = 2;
int count = COUNT;
int tugs = 7;

unsigned int seed;

Ship ships[COUNT];

static pthread_mutex_t mutex_tugs = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mutex_docks = PTHREAD_MUTEX_INITIALIZER;


void* run_ship(void* p){
    Ship* ship = (Ship*) p;

    while(1){
        if (ship->status == out){
            sleep(rand_r(&seed) % 20);
            ship->status = waitingForDock;
        }
        else if(ship->status == waitingForDock) {
            pthread_mutex_lock(&mutex_docks);
            if (dock_size) {
                pthread_mutex_lock(&mutex_tugs);
                if (tugs >= ship->tugs_count) {

                    dock_size--;
                    pthread_mutex_unlock(&mutex_docks);

                    ship->status = swimToDock;
                    tugs -= ship->tugs_count;
                    pthread_mutex_unlock(&mutex_tugs);
                    sleep(5);
                    ship->status = dock;
                    pthread_mutex_lock(&mutex_tugs);
                    tugs += ship->tugs_count;
                    pthread_mutex_unlock(&mutex_tugs);
                } else{
                    pthread_mutex_unlock(&mutex_tugs);
                    pthread_mutex_unlock(&mutex_docks);
                }

            } else
                pthread_mutex_unlock(&mutex_docks);
        }
        else if(ship->status == dock){
            sleep(3);
            ship->status = waitingInDock;
        }
        else if(ship->status == waitingInDock){
            pthread_mutex_lock(&mutex_tugs);
            if (tugs >= ship->tugs_count) {
                tugs -= ship->tugs_count;
                pthread_mutex_unlock(&mutex_tugs);

                pthread_mutex_lock(&mutex_docks);
                dock_size+=1;
                pthread_mutex_unlock(&mutex_docks);

                ship->status = swimmingOut;
                sleep(5);

                pthread_mutex_lock(&mutex_tugs);
                tugs += ship->tugs_count;
                pthread_mutex_unlock(&mutex_tugs);

                ship->status = out;
            }
            else
                pthread_mutex_unlock(&mutex_tugs);
        }
    }


}

void print_ships() {
    printf("docks: %2d tugs: %2d\n", dock_size, tugs);
    for (int i = 0; i < count; i++) {
        Ship* ship = &ships[i];
        printf("%d: %s\n", ship->id, STATUS_STRING[ship->status]);
    }
    printf("----------------------\n");
}

int main(){
    pthread_t threads[count];


    for(int i = 0; i < count; i++){
        ships[i] = default_Ship;
        ships[i].tugs_count = i+1;
        ships[i].id = i+1;

        pthread_create(&threads[i], NULL, run_ship, (void*)(&ships[i]));
    }
    while(1){
        print_ships();
        usleep(500000);
    }
}

